# Trie

This is a simple trie data structure that tries to use cacheline'd sized structures.
If more space is necessary, a second link is created at each node to extend the size.
