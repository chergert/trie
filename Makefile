all: test-trie

OBJECTS = trie.o

WARNINGS = -Wall -Werror
OPTIMIZE = -O0
DEBUG = -g
PKGS = glib-2.0

test-trie: $(OBJECTS) src/test.c
	$(CC) -o $@ $(WARNINGS) $(DEBUG) $(OPTIMIZE) $(shell pkg-config --cflags --libs $(PKGS)) src/test.c $(OBJECTS)

%.o: src/%.c src/%.h
	$(CC) -c -o $@ $(WARNINGS) $(DEBUG) $(OPTIMIZE) $(shell pkg-config --cflags $(PKGS)) src/$*.c

clean:
	rm -f $(OBJECTS) test-trie
