#include "trie.h"

#ifdef G_DISABLE_ASSERT
#undef G_DISABLE_ASSERT
#endif

static void
test_trie_insert (void)
{
   Trie *trie;

   trie = trie_new(NULL);
   trie_insert(trie, "a", "a");
   g_assert_cmpstr("a", ==, trie_lookup(trie, "a"));
   trie_insert(trie, "b", "b");
   g_assert_cmpstr("b", ==, trie_lookup(trie, "b"));
   trie_insert(trie, "c", "c");
   g_assert_cmpstr("c", ==, trie_lookup(trie, "c"));
   trie_insert(trie, "d", "d");
   g_assert_cmpstr("d", ==, trie_lookup(trie, "d"));
   trie_insert(trie, "e", "e");
   g_assert_cmpstr("e", ==, trie_lookup(trie, "e"));
   trie_insert(trie, "f", "f");
   g_assert_cmpstr("f", ==, trie_lookup(trie, "f"));
   trie_insert(trie, "g", "g");
   g_assert_cmpstr("g", ==, trie_lookup(trie, "g"));
   trie_destroy(trie);
}

static gboolean
traverse_cb (Trie        *trie,
             const gchar *key,
             gpointer     value,
             gpointer     user_data)
{
   guint *count = user_data;

   (*count)++;

   return FALSE;
}

static void
test_trie_gauntlet (void)
{
   gboolean ret;
   GTimer *timer;
   GError *error = NULL;
   gchar *content;
   gchar **words;
   guint word_count = 0;
   guint count = 0;
   guint i;
   guint j;
   Trie *trie;

   ret = g_file_get_contents("/usr/share/dict/words", &content, NULL, &error);
   g_assert_no_error(error);
   if (!ret) {
      g_assert(ret);
   }

   words = g_strsplit(content, "\n", -1);
   trie = trie_new(NULL);

   g_free(content);
   content = NULL;

   g_print("\ninsert,read,traverse,remove,free\n");

   timer = g_timer_new();

   for (i = 0; words[i]; i++) {
      trie_insert(trie, words[i], words[i]);
   }

   word_count = i;

   g_timer_stop(timer);
   g_print("%lf", g_timer_elapsed(timer, NULL));
   g_timer_reset(timer);

   for (j = 0; j < 4; j++) {
      for (i = 0; words[i]; i++) {
         gchar *s = trie_lookup(trie, words[i]);
         g_assert_cmpstr(words[i], ==, s);
      }
   }

   g_timer_stop(timer);
   g_print(",%lf", g_timer_elapsed(timer, NULL));
   g_timer_reset(timer);

   trie_traverse(trie, NULL,
                 G_PRE_ORDER, G_TRAVERSE_LEAVES, -1,
                 traverse_cb, &count);
   g_assert_cmpint(count, ==, word_count);

   g_timer_stop(timer);
   g_print(",%lf", g_timer_elapsed(timer, NULL));
   g_timer_reset(timer);

   for (i = 0; words[i]; i++) {
      if (i % 2 == 0) {
         g_assert(trie_remove(trie, words[i]));
      }
   }

   for (i = 0; words[i]; i++) {
      if (i % 2 != 0) {
         g_assert_cmpstr(words[i], ==, trie_lookup(trie, words[i]));
      } else {
         g_assert(!trie_lookup(trie, words[i]));
      }
   }

   g_timer_stop(timer);
   g_print(",%lf", g_timer_elapsed(timer, NULL));
   g_timer_reset(timer);

   trie_destroy(trie);
   trie = NULL;

   g_timer_stop(timer);
   g_print(",%lf\n", g_timer_elapsed(timer, NULL));

   g_strfreev(words);
   words = NULL;
}

gint
main (gint   argc,
      gchar *argv[])
{
   g_test_init(&argc, &argv, NULL);
   g_test_add_func("/Trie/insert", test_trie_insert);
   g_test_add_func("/Trie/gauntlet", test_trie_gauntlet);
   return g_test_run();
}
